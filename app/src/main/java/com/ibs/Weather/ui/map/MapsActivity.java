package com.ibs.Weather.ui.map;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ibs.Weather.PopScreenActivity;
import com.ibs.Weather.R;
import com.ibs.Weather.app.Constants;
import com.ibs.Weather.model.WeatherDetails;
import com.ibs.Weather.ui.BaseActivity;
import com.ibs.Weather.ui.placeDetails.ViewCityDetailsActivity;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MapsActivity extends BaseActivity implements PlacesView, OnMapReadyCallback, GoogleMap.OnMarkerClickListener, GoogleMap.OnInfoWindowClickListener {

    PlacesPresenter presenter;

    Marker previousMarkerClicked = null;
    boolean isChecking = true;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.fab)
    FloatingActionButton fab;
    List<WeatherDetails> weatherDetailses;
    private GoogleMap mMap;
    private String latitude;
    private String longitude;

    /*
    *launch the activity
     */
    public static void launch(Activity activity, String latitude, String longitude) {

        Intent i = new Intent(activity, MapsActivity.class);
        i.putExtra("latitude", latitude);
        i.putExtra("longitude", longitude);
        activity.startActivity(i);
        activity.finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        ButterKnife.bind(this);


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        // get location data
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            latitude = bundle.getString("latitude", null);
            longitude = bundle.getString("longitude", null);
        }


        //presenter
        presenter = new PlacesPresenterImpl();
        presenter.setView(this, this);

        fab.setVisibility(View.INVISIBLE);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewCityDetailsActivity.launch(MapsActivity.this, weatherDetailses);

            }
        });
    }

    /*

   *Weather view
     */

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng userLatLng = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
        mMap.addMarker(new MarkerOptions().position(userLatLng).title("My Location")
                //.icon(BitmapDescriptorFactory.fromResource(R.drawable.user_pin_ic))
        );
        mMap.moveCamera(CameraUpdateFactory.newLatLng(userLatLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(10.0f));
        mMap.setOnInfoWindowClickListener(this);

        mMap.setOnMarkerClickListener(this);
        presenter.getPlaces(latitude, longitude);
        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {

                if (!isChecking) {
                    isChecking = true;
                    presenter.getPlaces(String.valueOf(cameraPosition.target.latitude), String.valueOf(cameraPosition.target.longitude));

                }

            }
        });


    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
        isChecking = false;

    }

    @Override
    public void showPlaces(List<WeatherDetails> places) {
        int position = 0;
        mMap.clear();
        isChecking = false;
        fab.setVisibility(View.VISIBLE);

        weatherDetailses = new ArrayList<>();
        weatherDetailses.addAll(places);

        for (WeatherDetails weatherDetails : places) {

            Marker marker = addPlaceMarker(weatherDetails.coord.lat, weatherDetails.coord.lon,
                    (position == 0 ? R.drawable.marker_seletced_ic : R.drawable.marker_default_ic), weatherDetails);
            marker.showInfoWindow();

            marker.setTag(weatherDetails);// tag place object in marker


            if (position == 0)// to zoom to first marker
                previousMarkerClicked = marker;
            position++;

        }


        // animate to first marker at first position
        // mMap.animateCamera(CameraUpdateFactory.newLatLng(previousMarkerClicked.getPosition()), 250, null);

    }

    @Override
    public void showErrorMessage() {
    }


    /*
   *Marker
    */

    @Override
    public void showEmptyResult() {

    }

    private Marker addPlaceMarker(double latitude, double longitude, int icon, WeatherDetails weatherDetails) {


        View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.map_marker, null);
        TextView numTxt = (TextView) marker.findViewById(R.id.num_txt);
        final ImageView image = (ImageView) marker.findViewById(R.id.image);
        numTxt.setText(weatherDetails.main.temp + "ºC");


        Picasso.with(MapsActivity.this)
                .load(Constants.photoBaseUrl + weatherDetails.weather.get(0).icon + ".png")
                .into(new Target() {
                    @Override
                    public void onBitmapLoaded(final Bitmap nbitmap, Picasso.LoadedFrom from) {
            /* Save the bitmap or do something with it here */

                        //Set it in the ImageView
                        image.setImageBitmap(nbitmap);
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {

                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                });


        return mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(latitude, longitude)).title("" + weatherDetails.main.temp)
                        .anchor(0.5f, 0.5f)
                        .icon(BitmapDescriptorFactory
                                .fromBitmap(createDrawableFromView(
                                        this,
                                        marker)))
                //.icon(BitmapDescriptorFactory.fromResource(icon))
        );
    }


    public Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }


    @Override
    public boolean onMarkerClick(Marker marker) {

        WeatherDetails weatherDetails = (WeatherDetails) marker.getTag();
        if (weatherDetails != null) {    // if place =null,this is my location pin
            //  marker.showInfoWindow();


            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 10.0f));

            PopScreenActivity.launch(this, weatherDetails.name, weatherDetails.weather.get(0).description,
                    weatherDetails.main.temp, weatherDetails.main.temp_min, weatherDetails.main.temp_max,
                    weatherDetails.main.humidity, weatherDetails.main.pressure, weatherDetails.wind.speed, weatherDetails.wind.deg);
            return true;
        } else return false;
    }


    @Override
    public void onInfoWindowClick(Marker marker) {
        Toast.makeText(this, marker.getTitle() + " Info window clicked",
                Toast.LENGTH_SHORT).show();
    }
}
