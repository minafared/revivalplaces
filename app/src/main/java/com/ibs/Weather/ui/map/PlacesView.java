package com.ibs.Weather.ui.map;

import com.ibs.Weather.model.WeatherDetails;

import java.util.List;

/**
 * Created by mina fared on 07/10/2017.
 */

public interface PlacesView {

    void showLoading();

    void hideLoading();

    void showPlaces(List<WeatherDetails> venues);

    void showErrorMessage();

    void showEmptyResult();

}
