package com.ibs.Weather.ui;

import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;

import com.ibs.Weather.R;
import com.ibs.Weather.utils.NetworkStatusReceiver;
import com.ibs.Weather.utils.Utils;
import com.ibs.Weather.utils.location.LocationStatusReceiver;

public class BaseActivity extends FragmentActivity implements NetworkStatusReceiver.NetworkStatusListener, LocationStatusReceiver.LocationStatusListener {
    NetworkStatusReceiver networkStatusReceiver;
    LocationStatusReceiver locationStatusReceiver;
    private Snackbar snackbar;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected void onStart() {
        super.onStart();
        CheckingNetworkAndGPS();
    }

    @Override
    protected void onResume() {
        super.onResume();

        AddConnectivityListener();
        AddLocationListener();
    }

    @Override
    protected void onPause() {
        super.onPause();

        RemoveConnectivityListener();
        RemoveLocationListener();
    }

    @Override
    protected void onStop() {
        super.onStop();

    }


    /*
    * super function when all listeners are ok
     */
    @CallSuper
    public void WhenAllIsGood() {

    }

    /*
    *check function
     */

    public void CheckingNetworkAndGPS() {
        if (Utils.isOnline(this) && Utils.isGpsEnabled(this)) {
            WhenAllIsGood();
            HideReloadBar();

        } else {

            if (!Utils.isOnline(this)) {
                loadOfflineData();
                ShowReloadBar("No internet connection!");

            }

            if (!Utils.isGpsEnabled(this))
                ShowReloadBar("GPS is disabled");


        }
    }


    /*
    * Network
     */
    void AddConnectivityListener() {
        networkStatusReceiver = new NetworkStatusReceiver();
        registerReceiver(networkStatusReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    void RemoveConnectivityListener() {
        unregisterReceiver(networkStatusReceiver);
    }

    @Override
    public void onNetworkFail() {
        CheckingNetworkAndGPS();
    }

    @Override
    public void onNetworkConnected() {
        CheckingNetworkAndGPS();
    }



    /*
    *Location
     */

    void AddLocationListener() {
        locationStatusReceiver = new LocationStatusReceiver();
        registerReceiver(locationStatusReceiver, new IntentFilter("android.location.PROVIDERS_CHANGED"));
    }

    void RemoveLocationListener() {
        unregisterReceiver(locationStatusReceiver);
    }


    @Override
    public void onLocationFail() {
        CheckingNetworkAndGPS();
    }

    @Override
    public void onLocationSuccess() {
        CheckingNetworkAndGPS();
    }




    /*
    *Snackbar

     */

    public void ShowReloadBar(String msg) {
        snackbar = Snackbar
                .make(findViewById(android.R.id.content), msg, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.retry, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                CheckingNetworkAndGPS();
                            }
                        }, 1000);
                    }
                });

        // Changing message text color
        snackbar.setActionTextColor(ContextCompat.getColor(this, R.color.colorAccent));

        // Changing action button text color
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.YELLOW);
        // change snack bar background
        sbView.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));

        snackbar.show();
    }

    public void HideReloadBar() {
        if (snackbar != null)
            snackbar.dismiss();
    }

    @CallSuper
    public void loadOfflineData() {

    }
}