package com.ibs.Weather.ui.placeDetails;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.ibs.Weather.R;
import com.ibs.Weather.model.WeatherDetails;

import java.io.Serializable;
import java.util.List;

public class ViewCityDetailsActivity extends AppCompatActivity {

    List<WeatherDetails> weatherDetailses;
    private ListView listView;
    private ListViewAdapter adapter;

    public static void launch(Activity activity, List<WeatherDetails> places) {

        Intent i = new Intent(activity, ViewCityDetailsActivity.class);
        i.putExtra("weatherDetailses", (Serializable) places);
        activity.startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_place);
        listView = (ListView) findViewById(R.id.list_item);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);





        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            weatherDetailses = (List<WeatherDetails>) bundle.getSerializable("weatherDetailses");
        }

        adapter = new ListViewAdapter(this, R.layout.item_listview, weatherDetailses);
        listView.setAdapter(adapter);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);

        MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (TextUtils.isEmpty(newText)) {
                    adapter.filter("");
                    listView.clearTextFilter();
                } else {
                    adapter.filter(newText);
                }
                return true;
            }
        });

        return true;
    }

}
