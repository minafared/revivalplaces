package com.ibs.Weather.ui.map;

import android.content.Context;

/**
 * Created by mina fared on 07/10/2017.
 */

public interface PlacesPresenter {

    void setView(Context context, PlacesView view);

    void getPlaces( String latitude, String longitude);
 }
