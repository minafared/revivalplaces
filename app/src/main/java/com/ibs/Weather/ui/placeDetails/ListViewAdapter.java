package com.ibs.Weather.ui.placeDetails;

/**
 * Created by mina fared on 10/16/2017.
 */

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ibs.Weather.PopScreenActivity;
import com.ibs.Weather.R;
import com.ibs.Weather.model.WeatherDetails;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ListViewAdapter extends ArrayAdapter<WeatherDetails> {

    List<WeatherDetails> weatherDetailses;
    List<WeatherDetails> weatherDetailsessearchList;
    private ViewCityDetailsActivity activity;

    public ListViewAdapter(ViewCityDetailsActivity context, int resource, List<WeatherDetails> weatherDetailses) {
        super(context, resource, weatherDetailses);
        this.activity = context;
        this.weatherDetailses = weatherDetailses;
        this.weatherDetailsessearchList = new ArrayList<>();
        this.weatherDetailsessearchList.addAll(weatherDetailses);
    }

    @Override
    public int getCount() {
        return weatherDetailses.size();
    }

    @Override
    public WeatherDetails getItem(int position) {
        return weatherDetailses.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        // If holder not exist then locate all view from UI file.
        if (convertView == null) {
            // inflate UI from XML file
            convertView = inflater.inflate(R.layout.item_listview, parent, false);
            // get all UI view
            holder = new ViewHolder(convertView);
            // set tag for holder
            convertView.setTag(holder);
        } else {
            // if holder created, get tag from view
            holder = (ViewHolder) convertView.getTag();
        }

        holder.cityName_tv.setText(getItem(position).name);

        holder.currentTemp_tv.setText(getItem(position).main.temp + "ºC");

        holder.humidity_tv.setText(getItem(position).main.humidity + "%");
        holder. wind_tv.setText(getItem(position).wind.speed + " km/h " + getItem(position).wind.deg  + "º");
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopScreenActivity.launch(activity, getItem(position).name, getItem(position).weather.get(0).description,
                        getItem(position).main.temp, getItem(position).main.temp_min, getItem(position).main.temp_max,
                        getItem(position).main.humidity, getItem(position).main.pressure, getItem(position).wind.speed,
                        getItem(position).wind.deg);

            }
        });

        return convertView;
    }

    // Filter method
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        weatherDetailses.clear();
        if (charText.length() == 0) {
            weatherDetailses.addAll(weatherDetailsessearchList);
        } else {
            for (WeatherDetails s : weatherDetailsessearchList) {
                if (s.name.toLowerCase(Locale.getDefault()).contains(charText)) {
                    weatherDetailses.add(s);
                }
            }
        }
        notifyDataSetChanged();
    }

    private class ViewHolder {
         private TextView cityName_tv,currentTemp_tv,wind_tv,humidity_tv;

        public ViewHolder(View v) {
            cityName_tv = (TextView) v.findViewById(R.id.cityName_tv);
            currentTemp_tv = (TextView) v.findViewById(R.id.currentTemp_tv);
            humidity_tv = (TextView) v.findViewById(R.id.humidity_tv);
            wind_tv = (TextView) v.findViewById(R.id.wind_tv);

        }
    }
}