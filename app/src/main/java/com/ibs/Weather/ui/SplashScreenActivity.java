package com.ibs.Weather.ui;

import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.ibs.Weather.R;
import com.ibs.Weather.ui.map.MapsActivity;
import com.ibs.Weather.utils.UserPreferences;
import com.ibs.Weather.utils.location.LocationService;
import com.ibs.Weather.utils.location.onLocationListener;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mina fared on 7/10/2017.
 */
public class SplashScreenActivity extends BaseActivity implements onLocationListener {

    LocationService locationService;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        ButterKnife.bind(this);
        UserPreferences userPreferences = new UserPreferences(this);
        //MapsActivity.launch(this, String.valueOf(userPreferences.getLatitude()), String.valueOf(userPreferences.getLongitude()));

    }


     /*
     * super functions from baseActivity
     */

    @Override
    public void WhenAllIsGood() {
        super.WhenAllIsGood();
        // getLocation
        getCurrentLocation();
    }


    @Override
    public void onLocationChanged(Location location, boolean isSwipeRefreshLayout) {
        progressBar.setVisibility(View.INVISIBLE);
        MapsActivity.launch(this, String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()));
    }


    private void getCurrentLocation() {
        if (locationService == null)
            locationService = new LocationService(this, this, false);
        else
            locationService.getLocation();


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (locationService != null) {
            locationService.stopUpdateUserLocations();
            locationService = null;
        }

    }
}
