package com.ibs.Weather.utils.location;

import android.location.Location;

/**
 * Created by mina fared on 08/10/2017.
 */


public interface onLocationListener {

      void onLocationChanged(Location location, boolean isSwipeRefreshLayout);
}
