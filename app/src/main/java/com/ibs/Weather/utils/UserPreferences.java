package com.ibs.Weather.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.ibs.Weather.app.Constants;

/**
 * Created by mina fared on 07/10/2017.
 */


public class UserPreferences {
    SharedPreferences mPref;
    SharedPreferences.Editor editor;

    public UserPreferences(Context mContext) {
        mPref = mContext.getSharedPreferences(Constants.PREFS, Context.MODE_PRIVATE);
        editor = mPref.edit();
    }

    public void setLocation(String latitude, String longitude) {
        SharedPreferences.Editor editor = mPref.edit();
        editor.putString("latitude", latitude);
        editor.putString("longitude", longitude);
        editor.apply();
    }

    public void setUserMode(int mode) {
        SharedPreferences.Editor editor = mPref.edit();
        editor.putInt("mode", mode);
        editor.apply();
    }

    public String getLatitude() {
        return mPref.getString("latitude", "");
    }

    public String getLongitude() {
        return mPref.getString("longitude", "");
    }


}
