package com.ibs.Weather.utils.location;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.ibs.Weather.R;
import com.ibs.Weather.app.Constants;
import com.ibs.Weather.utils.UserPreferences;


/**
 * Created by mina fared on 8/10/2017.
 */

public class LocationService extends Service implements LocationListener {

    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 500; // 500 meters min distance to update
    private static final long MIN_TIME_BW_UPDATES = 3000 * 10 * 1; // 30 seconds to check
    private final Context mContext;
    protected LocationManager locationManager;
    boolean isGPSEnabled, isNetworkEnabled, canGetLocation;
    Location location;
    double latitude, longitude;
    onLocationListener listener;
    UserPreferences userPreferences;
    boolean isSwipeRefreshLayout;

    public LocationService(Context context, onLocationListener listener, boolean isSwipeRefreshLayout) {
        this.mContext = context;
        this.listener = listener;
        this.isSwipeRefreshLayout = isSwipeRefreshLayout;
        userPreferences = new UserPreferences(context);
        getLocation();
    }

    public Location getLocation() {
        try {
            locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);

            // getting GPS status
            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
                showSettingsAlert();
            } else {
                this.canGetLocation = true;
                // First get location from Network Provider
                if (isNetworkEnabled) {

                    if (ContextCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                        ActivityCompat.requestPermissions((Activity) mContext, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION},
                                Constants.LOCATION_PERMISSION_CODE);
                    }

                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    Log.d("Network", "Network");
                    if (locationManager != null) {
                        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                            userPreferences.setLocation(String.valueOf(latitude), String.valueOf(longitude));

                        }
                    }
                }
                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (location == null) {
                        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                        Log.d("GPS Enabled", "GPS Enabled");
                        if (locationManager != null) {
                            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                                userPreferences.setLocation(String.valueOf(latitude), String.valueOf(longitude));
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return location;
    }

    /**
     * Stop using GPS listener
     */
    public void stopUpdateUserLocations() {

        // to remover GPS_PROVIDER listener

        if (locationManager != null) {
            locationManager.removeUpdates(LocationService.this);
        }

        // to remover NETWORK_PROVIDER listener
        if (locationManager != null) {
            locationManager.removeUpdates(LocationService.this);
        }
    }


    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
        alertDialog.setTitle(R.string.gpsAlertTitle);
        alertDialog
                .setMessage(R.string.gpsAlertMessage);
        alertDialog.setPositiveButton(R.string.gpsAlertPositiveButton,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(
                                Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        mContext.startActivity(intent);
                    }
                });

        alertDialog.setNegativeButton(R.string.gpsAlertNegativeButton,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alertDialog.show();
    }

    @Override
    public void onLocationChanged(Location location) {

        if (location != null) {
            listener.onLocationChanged(location, isSwipeRefreshLayout);

            userPreferences.setLocation(String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()));
//            Toast.makeText(mContext, distanceDifference(location.getLatitude(), location.getLongitude()) + "",
//                    Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }


    private float distanceDifference(double latitude, double longitude) {
        Location locationA = new Location("Location A");

        locationA.setLatitude(latitude);
        locationA.setLongitude(longitude);

        Location locationB = new Location("Location B");

        locationB.setLatitude(Double.parseDouble(userPreferences.getLatitude()));
        locationB.setLongitude(Double.parseDouble(userPreferences.getLongitude()));

        return locationA.distanceTo(locationB);
    }
}