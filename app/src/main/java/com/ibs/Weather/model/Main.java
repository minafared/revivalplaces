package com.ibs.Weather.model;

import java.io.Serializable;

/**
 * Created by mina fared on 10/17/2017.
 */
public class Main implements Serializable {
    public double temp;
    public double pressure;
    public int humidity;
    public double temp_min;
    public double temp_max;
    public double sea_level;
    public double grnd_level;
}
