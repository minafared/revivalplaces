package com.ibs.Weather.model;

import java.io.Serializable;

/**
 * Created by mina fared on 10/17/2017.
 */
public class Weather implements Serializable {
    public int id;
    public String main;
    public String description;
    public String icon;
}
