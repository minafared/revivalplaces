package com.ibs.Weather;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PopScreenActivity extends AppCompatActivity {

    String cityName, weatherDetails;
    Double currentTemp, minTemp, pressure, windSpeed, maxTemp, windDeg;
    int humidity;

    @BindView(R.id.cityName_tv)
    TextView cityName_tv;

    @BindView(R.id.weatherDetails_tv)
    TextView weatherDetails_tv;

    @BindView(R.id.currentTemp_tv)
    TextView currentTemp_tv;

    @BindView(R.id.maxTemp_tv)
    TextView maxTemp_tv;

    @BindView(R.id.minTemp_tv)
    TextView minTemp_tv;

    @BindView(R.id.humidity_tv)
    TextView humidity_tv;

    @BindView(R.id.pressure_tv)
    TextView pressure_tv;

    @BindView(R.id.wind_tv)
    TextView wind_tv;


    public static void launch(Activity activity, String cityName, String weatherDetails, double currentTemp, double minTemp, double maxTemp,
                              int humidity, double pressure, double windSpeed, double windDeg) {

        Intent i = new Intent(activity, PopScreenActivity.class);
        i.putExtra("cityName", cityName);
        i.putExtra("weatherDetails", weatherDetails);
        i.putExtra("currentTemp", currentTemp);
        i.putExtra("minTemp", minTemp);
        i.putExtra("humidity", humidity);
        i.putExtra("pressure", pressure);
        i.putExtra("windSpeed", windSpeed);
        i.putExtra("windDeg", windDeg);
        i.putExtra("maxTemp", maxTemp);
        activity.startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pop_screen);
        ButterKnife.bind(this);


        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            cityName = bundle.getString("cityName", null);
            weatherDetails = bundle.getString("weatherDetails", null);

            currentTemp = bundle.getDouble("currentTemp", 0);
            minTemp = bundle.getDouble("minTemp", 0);
            maxTemp = bundle.getDouble("maxTemp", 0);

            pressure = bundle.getDouble("pressure", 0);

            windSpeed = bundle.getDouble("windSpeed", 0);
            windDeg = bundle.getDouble("windDeg", 0);

            humidity = bundle.getInt("humidity", 0);

        }


        cityName_tv.setText(cityName);
        weatherDetails_tv.setText(weatherDetails);

        currentTemp_tv.setText(currentTemp + "ºC");
        maxTemp_tv.setText("Max "+maxTemp + "ºC");
        minTemp_tv.setText("Min "+minTemp + "ºC");

        humidity_tv.setText(humidity + "%");
        pressure_tv.setText(pressure + " mb");
        wind_tv.setText(windSpeed + " km/h " + windDeg + "º");
    }
}
